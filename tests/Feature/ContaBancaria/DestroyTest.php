<?php

namespace Tests\Feature\ContaBancaria;


use App\Models\ContaBancaria;
use App\Models\User;
use Tests\TestCase;

class DestroyTest extends TestCase
{
    private const ROTA = 'conta-banco.destroy';

    public function test_sucesso()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['delete'])->plainTextToken;

        $response = $this->withToken($token)
            ->deleteJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(200)
            ->assertSee('');
    }

    public function test_falha_permissao()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['sem-permissao'])->plainTextToken;

        $response = $this->withToken($token)
            ->deleteJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(403)
            ->assertJsonFragment(["message" => ""]);
    }

    public function test_falha_autenticacao()
    {
        $contaBancaria = ContaBancaria::factory()->create();

        $response = $this->deleteJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(401)
            ->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
