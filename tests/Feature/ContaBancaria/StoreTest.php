<?php

namespace Tests\Feature\ContaBancaria;


use App\Models\ContaBancaria;
use App\Models\User;
use Tests\TestCase;

class StoreTest extends TestCase
{
    private const ROTA = 'conta-banco.store';

    public function test_sucesso()
    {
        $novosDados = ContaBancaria::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['create'])->plainTextToken;

        $response = $this->withToken($token)
            ->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(201)
            ->assertJsonFragment([
                "data" => [
                    "conta_bancaria_id" => $response['data']['conta_bancaria_id'],
                    "banco_nome"        => $novosDados['banco_nome'],
                    "agencia_numero"    => $novosDados['agencia_numero'],
                    "conta_numero"      => $novosDados['conta_numero'],
                    "saldo_inicial"     => $novosDados['saldo_inicial'],
                    "created_at"        => $response['data']['created_at'],
                    "updated_at"        => $response['data']['updated_at'],
                ]
            ]);
        $this->assertDatabaseHas('conta_bancaria', $novosDados);
    }

    public function test_falha_permissao()
    {
        $token = User::factory()->create()->createToken('token-name', ['sem-permissao'])->plainTextToken;

        $response = $this->withToken($token)
            ->postJson(route(self::ROTA));

        $response->assertStatus(403)
            ->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_falha_autenticacao()
    {
        $response = $this->postJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
