<?php

namespace Tests\Feature\ContaBancaria;


use App\Models\ContaBancaria;
use App\Models\User;
use Tests\TestCase;

class ShowTest extends TestCase
{
    private const ROTA = 'conta-banco.show';

    public function test_sucesso()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['read'])->plainTextToken;

        $response = $this->withToken($token)
            ->getJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    "conta_bancaria_id" => $contaBancaria->conta_bancaria_id,
                    "banco_nome"        => $contaBancaria->banco_nome,
                    "agencia_numero"    => $contaBancaria->agencia_numero,
                    "conta_numero"      => $contaBancaria->conta_numero,
                    "saldo_inicial"     => $contaBancaria->saldo_inicial,
                    "created_at"        => $contaBancaria->created_at,
                    "updated_at"        => $contaBancaria->updated_at,
                ]
            ]);
    }

    public function test_falha_permissao()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['sem-permissao'])->plainTextToken;

        $response = $this->withToken($token)
            ->getJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(403)
            ->assertJsonFragment(["message" => ""]);
    }

    public function test_falha_autenticacao()
    {
        $contaBancaria = ContaBancaria::factory()->create();

        $response = $this->getJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(401)
            ->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
