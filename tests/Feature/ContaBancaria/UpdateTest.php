<?php

namespace Tests\Feature\ContaBancaria;


use App\Models\ContaBancaria;
use App\Models\User;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    private const ROTA = 'conta-banco.update';

    public function test_sucesso()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $novosDados = ContaBancaria::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $response = $this->withToken($token)
            ->putJson(route(self::ROTA, $contaBancaria->getKey()), $novosDados);

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    "conta_bancaria_id" => $contaBancaria->getKey(),
                    "banco_nome"        => $novosDados['banco_nome'],
                    "agencia_numero"    => $novosDados['agencia_numero'],
                    "conta_numero"      => $novosDados['conta_numero'],
                    "saldo_inicial"     => $novosDados['saldo_inicial'],
                    "created_at"        => $contaBancaria->created_at,
                    "updated_at"        => $contaBancaria->updated_at,
                ]
            ]);
    }

    public function test_falha_permissao()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['sem-permissao'])->plainTextToken;

        $response = $this->withToken($token)
            ->putJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(403)
            ->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_falha_autenticacao()
    {
        $contaBancaria = ContaBancaria::factory()->create();
        $response = $this->putJson(route(self::ROTA, $contaBancaria->getKey()));

        $response->assertStatus(401)
            ->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
