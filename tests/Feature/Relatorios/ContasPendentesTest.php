<?php

namespace Tests\Feature\Relatorios;

use App\Models\Fornecedor;
use App\Models\User;
use Tests\TestCase;

class ContasPendentesTest extends TestCase
{
    private const ROTA = 'relatorio.contas-pendentes';

    public function test_sucesso_json()
    {
        $fornecedor = Fornecedor::factory()->hasContasPagar()->create();
        $contaPagar = $fornecedor->contasPagar()->first();
        $token = User::factory()->create()->createToken('token-name', ['report'])->plainTextToken;

        $response = $this->withToken($token)
            ->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertHeader('Content-Type', 'application/json')
            ->assertExactJson([
                [
                    "conta_pagar_id" => $contaPagar->conta_pagar_id,
                    "fornecedor_id"  => $contaPagar->fornecedor_id,
                    "descricao"      => $contaPagar->descricao,
                    "valor"          => $contaPagar->valor,
                    "paga_em"        => $contaPagar->paga_em,
                    "created_at"     => $contaPagar->created_at,
                    "updated_at"     => $contaPagar->updated_at,
                ]
            ]);
    }

    public function test_sucesso_pdf()
    {
        $token = User::factory()->create()->createToken('token-name', ['report'])->plainTextToken;

        $response = $this->withToken($token)
            ->withHeaders([
                'Content-Type' => 'application/json',
                'Accept'       => 'application/pdf',
            ])
            ->get(route(self::ROTA));

        $response->assertStatus(200)
            ->assertHeader('Content-Type', 'application/pdf');
    }

    public function test_falha_permissao()
    {
        $token = User::factory()->create()->createToken('token-name', ['sem-permissao'])->plainTextToken;

        $response = $this->withToken($token)
            ->getJson(route(self::ROTA));

        $response->assertStatus(403)
            ->assertJsonFragment(["message" => ""]);
    }

    public function test_falha_autenticacao()
    {
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
