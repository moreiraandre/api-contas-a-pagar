<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContaPagar extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'conta_pagar';
    protected $primaryKey = 'conta_pagar_id';
    protected $fillable = [
        'fornecedor_id',
        'descricao',
        'valor',
    ];
    protected $casts = [
        'valor'         => 'float',
        'fornecedor_id' => 'integer',
    ];
    protected $hidden = ['deleted_at'];
    protected $dates = ['paga_em'];

    public function historico()
    {
        return $this->hasMany(ContaPagarHistorico::class, 'conta_pagar_id', 'conta_pagar_id');
    }
}
