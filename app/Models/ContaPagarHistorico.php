<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContaPagarHistorico extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'conta_pagar_historico';
    protected $primaryKey = 'conta_pagar_historico_id';
    protected $casts = [
        'users_id'      => 'integer',
        'is_liquidacao' => 'bool',
    ];
    protected $fillable = [
        'is_liquidacao',
        'users_id',
    ];

    public function usuario()
    {
        return $this->hasOne(User::class, 'id', 'users_id');
    }
}
