<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'fornecedor';
    protected $primaryKey = 'fornecedor_id';
    protected $fillable = [
        'nome',
        'endereco',
        'telefone',
        'email',
    ];

    public function contasPagar()
    {
        return $this->hasMany(ContaPagar::class, 'fornecedor_id', 'fornecedor_id');
    }
}
