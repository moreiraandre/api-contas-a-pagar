<?php

namespace App\Http\Controllers;

use App\Models\ContaPagar;
use Dominio\ContaPagar\LiquidarEstornar as Dominio;
use App\Http\Resources\ContaPagar as Resource;

class ContaPagarLiquidarController extends Controller
{
    /**
     * Liquidar Conta a Pagar
     *
     * Realiza a liquidação de uma conta a pagar
     * @group Conta a Pagar Liquidar/Estornar
     * @urlParam conta_pagar integer required O valor de conta_pagar_id
     * @responseFile Response/ContaPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\ContaPagar] 4"}
     */
    public function __invoke(ContaPagar $contaPagar, Dominio $dominio)
    {
        $dominio->liquidar($contaPagar);
        return new Resource($contaPagar);
    }
}
