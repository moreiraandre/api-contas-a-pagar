<?php

namespace App\Http\Controllers;

use Dominio\ContaPagar\Relatorio;

class ContaPagarRelatorioController extends Controller
{
    /**
     * Contas Pendentes
     *
     * Retorna a listagem de contas pendentes de pagamento. Utilize cabeçalho Accept: application/json ou application/pdf
     * @group Conta a Pagar Relatório
     * @responseFile Response/ContaPagar/Relatorio/Pendentes.json
     */
    public function contasPendentes(Relatorio $relatorio)
    {
        abort_if(!auth()->user()->tokenCan('report'), 403);
        return $relatorio->contasPendentes();
    }

    /**
     * Contas Liquidadas
     *
     * Retorna a listagem de contas liquidadas. Utilize cabeçalho Accept: application/json ou application/pdf
     * @group Conta a Pagar Relatório
     * @responseFile Response/ContaPagar/Relatorio/Liquidadas.json
     */
    public function contasLiquidadas(Relatorio $relatorio)
    {
        return $relatorio->contasLiquidadas();
    }
}
