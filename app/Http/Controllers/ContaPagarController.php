<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContaPagarStore;
use App\Http\Requests\ContaPagarUpdate;
use App\Http\Resources\ContaPagar as Resource;
use App\Models\ContaPagar as Model;

class ContaPagarController extends Controller
{
    /**
     * Listar Contas a Pagar
     *
     * Retorna todos os registros do banco
     * @group Conta a Pagar
     * @responseFile Response/ContaPagar/Listar.json
     */
    public function index()
    {
        return Resource::collection(Model::with('historico.usuario')->paginate(5));
    }

    /**
     * Criar Conta a Pagar
     *
     * Cria uma nova conta a pagar
     * @group Conta a Pagar
     * @responseFile 201 Response/ContaPagar/Detalhar.json
     * @responseFile 422 Response/ContaPagar/ValidarCriar.json
     */
    public function store(ContaPagarStore $request)
    {
        $novo = Model::create($request->all());
        return new Resource($novo);
    }

    /**
     * Detalhar Conta a Pagar
     *
     * Retorna os dados da conta a pagar
     * @group Conta a Pagar
     * @urlParam conta_pagar integer required O valor de conta_pagar_id
     * @responseFile Response/ContaPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\ContaPagar] 4"}
     */
    public function show(Model $contaPagar)
    {
        return new Resource($contaPagar);
    }

    /**
     * Atualizar Conta a Pagar
     *
     * Atualiza os dados do contaPagar
     * @group Conta a Pagar
     * @urlParam conta_pagar integer required O valor de conta_pagar_id
     * @responseFile Response/ContaPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\ContaPagar] 4"}
     * @responseFile 422 Response/ContaPagar/ValidarAtualizar.json
     */
    public function update(ContaPagarUpdate $request, Model $contaPagar)
    {
        $contaPagar->update($request->all());
        return new Resource($contaPagar);
    }

    /**
     * Excluir Conta a Pagar
     *
     * Exclui um contaPagar
     * @group Conta a Pagar
     * @urlParam conta_pagar integer required O valor de conta_pagar_id
     * @response
     * @response 404 {"message": "No query results for model [App\\Models\\ContaPagar] 4"}
     */
    public function destroy(Model $contaPagar)
    {
        $contaPagar->delete();
    }
}
