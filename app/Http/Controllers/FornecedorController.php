<?php

namespace App\Http\Controllers;

use App\Http\Requests\FornecedorStore;
use App\Http\Requests\FornecedorUpdate;
use App\Http\Resources\Fornecedor as Resource;
use App\Models\Fornecedor as Model;
use Dominio\Fornecedor\Cadastrar;

class FornecedorController extends Controller
{
    private Cadastrar $cadastrar;

    public function __construct(Cadastrar $cadastrar) {
        $this->cadastrar = $cadastrar;
    }

    /**
     * Listar Fornecedores
     *
     * Retorna todos os registros do banco
     * @group Fornecedor
     * @responseFile Response/Fornecedor/Listar.json
     */
    public function index()
    {
        return Resource::collection(Model::with('contasPagar')->paginate(5));
    }

    /**
     * Criar Fornecedor
     *
     * Cria um novo fornecedor
     * @group Fornecedor
     * @responseFile 201 Response/Fornecedor/Detalhar.json
     * @responseFile 422 Response/Fornecedor/ValidarCriar.json
     */
    public function store(FornecedorStore $request)
    {
        $novo = $this->cadastrar->cadastrar($request);
        return new Resource($novo);
    }

    /**
     * Detalhar Fornecedor
     *
     * Retorna os dados do fornecedor
     * @group Fornecedor
     * @urlParam fornecedor integer required O valor de fornecedor_id
     * @responseFile Response/Fornecedor/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Fornecedor] 4"}
     */
    public function show(Model $fornecedor)
    {
        return new Resource($fornecedor);
    }

    /**
     * Atualizar Fornecedor
     *
     * Atualiza os dados do fornecedor
     * @group Fornecedor
     * @urlParam fornecedor integer required O valor de fornecedor_id
     * @responseFile Response/Fornecedor/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Fornecedor] 4"}
     * @responseFile 422 Response/Fornecedor/ValidarAtualizar.json
     */
    public function update(FornecedorUpdate $request, Model $fornecedor)
    {
        $fornecedor->update($request->all());
        return new Resource($fornecedor);
    }

    /**
     * Excluir Fornecedor
     *
     * Exclui um fornecedor
     * @group Fornecedor
     * @urlParam fornecedor integer required O valor de fornecedor_id
     * @response
     * @response 404 {"message": "No query results for model [App\\Models\\Fornecedor] 4"}
     */
    public function destroy(Model $fornecedor)
    {
        $fornecedor->delete();
    }
}
