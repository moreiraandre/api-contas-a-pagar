<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContaBancariaStore;
use App\Http\Requests\ContaBancariaUpdate;
use App\Http\Resources\ContaBancaria as Resource;
use App\Models\ContaBancaria as Model;

class ContaBancariaController extends Controller
{
    /**
     * Listar Contas Bancaria
     *
     * Retorna todos os registros do banco
     * @group Conta Bancaria
     * @responseFile Response/ContaBancaria/Listar.json
     */
    public function index()
    {
        abort_if(!auth()->user()->tokenCan('read'), 403);
        return Resource::collection(Model::paginate(5));
    }

    /**
     * Criar Conta Bancaria
     *
     * Cria uma nova conta bancaria
     * @group Conta Bancaria
     * @responseFile 201 Response/ContaBancaria/Detalhar.json
     * @responseFile 422 Response/ContaBancaria/ValidarCriar.json
     */
    public function store(ContaBancariaStore $request)
    {
        $novo = Model::create($request->all());
        return new Resource($novo);
    }

    /**
     * Detalhar Conta Bancaria
     *
     * Retorna os dados da conta bancaria
     * @group Conta Bancaria
     * @urlParam conta_banco integer required O valor de conta_bancaria_id
     * @responseFile Response/ContaBancaria/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\ContaBancaria] 4"}
     */
    public function show(Model $contaBanco)
    {
        abort_if(!auth()->user()->tokenCan('read'), 403);
        return new Resource($contaBanco);
    }

    /**
     * Atualizar Conta Bancaria
     *
     * Atualiza os dados da conta bancaria
     * @group Conta Bancaria
     * @urlParam conta_banco integer required O valor de conta_bancaria_id
     * @responseFile Response/ContaBancaria/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\ContaBancaria] 4"}
     * @responseFile 422 Response/ContaBancaria/ValidarAtualizar.json
     */
    public function update(ContaBancariaUpdate $request, Model $contaBanco)
    {
        $contaBanco->update($request->all());
        return new Resource($contaBanco);
    }

    /**
     * Excluir Conta Bancaria
     *
     * Exclui uma conta bancaria
     * @group Conta Bancaria
     * @urlParam conta_banco integer required O valor de conta_bancaria_id
     * @response
     * @response 404 {"message": "No query results for model [App\\Models\\ContaBancaria] 4"}
     */
    public function destroy(Model $contaBanco)
    {
        abort_if(!auth()->user()->tokenCan('delete'), 403);
        $contaBanco->delete();
    }
}
