<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContaPagar extends JsonResource
{
    public function toArray($request)
    {
        return [
            'conta_pagar_id' => $this->getKey(),
            'fornecedor_id'  => $this->fornecedor_id,
            'descricao'      => $this->descricao,
            'valor'          => $this->valor,
            'paga_em'        => $this->paga_em,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'historico'      => ContaPagarHistorico::collection($this->historico),
        ];
    }
}
