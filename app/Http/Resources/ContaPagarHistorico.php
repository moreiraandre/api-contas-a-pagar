<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContaPagarHistorico extends JsonResource
{
    public function toArray($request)
    {
        return [
            'conta_pagar_historico_id' => $this->getKey(),
            'users_id'                 => $this->users_id,
            'users_name'               => $this->usuario->name,
            'is_liquidacao'            => $this->is_liquidacao,
            'created_at'               => $this->created_at,
            'updated_at'               => $this->updated_at,
        ];
    }
}
