<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Fornecedor extends JsonResource
{
    public function toArray($request)
    {
        return [
            'fornecedor_id' => $this->getKey(),
            'nome'          => $this->nome,
            'endereco'      => $this->endereco,
            'telefone'      => $this->telefone,
            'email'         => $this->email,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'contas_pagar'  => ContaPagar::collection($this->contasPagar),
        ];
    }
}
