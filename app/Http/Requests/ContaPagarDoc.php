<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class ContaPagarDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'fornecedor_id' => [
                'description' => 'O valor de fornecedor_id. No endpoint GET /api/fornecedor.',
                'example'     => 1,
            ],
            'descricao'     => [
                'description' => 'A descrição da conta.',
                'example'     => 'CONTA DE TELEFONE',
            ],
            'valor'         => [
                'description' => 'O valor da conta.',
                'example'     => 121.12,
            ],
        ];
    }
}
