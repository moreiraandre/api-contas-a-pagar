<?php

namespace App\Http\Requests;

class ContaBancariaUpdate extends ContaBancariaDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('update');
    }

    public function rules()
    {
        return [
            'banco_nome'     => ['filled', 'max:255'],
            'agencia_numero' => ['filled', 'max:10'],
            'conta_numero'   => ['filled', 'max:10'],
            'saldo_inicial'  => ['filled', 'numeric'],
        ];
    }
}
