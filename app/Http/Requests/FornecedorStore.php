<?php

namespace App\Http\Requests;

class FornecedorStore extends FornecedorDoc
{
    public function rules()
    {
        return [
            'nome'                     => ['required', 'max:255'],
            'endereco'                 => ['required', 'max:255'],
            'telefone'                 => ['required', 'max:20'],
            'email'                    => ['nullable', 'max:20', 'email'],
            'contas_pagar'             => ['filled', 'array'],
            'contas_pagar.*.descricao' => ['required', 'max:255'],
            'contas_pagar.*.valor'     => ['required', 'numeric'],
        ];
    }
}
