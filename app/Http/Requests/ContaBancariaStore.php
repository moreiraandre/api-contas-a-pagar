<?php

namespace App\Http\Requests;

class ContaBancariaStore extends ContaBancariaDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('create');
    }

    public function rules()
    {
        return [
            'banco_nome'     => ['required', 'max:255'],
            'agencia_numero' => ['required', 'max:10'],
            'conta_numero'   => ['required', 'max:10'],
            'saldo_inicial'  => ['required', 'numeric'],
        ];
    }
}
