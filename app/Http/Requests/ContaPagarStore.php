<?php

namespace App\Http\Requests;

class ContaPagarStore extends ContaPagarDoc
{
    public function rules()
    {
        return [
            'fornecedor_id' => ['required', 'integer', 'exists:fornecedor'],
            'descricao'     => ['required', 'max:255'],
            'valor'         => ['required', 'numeric'],
        ];
    }
}
