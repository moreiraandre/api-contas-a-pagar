<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class FornecedorDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'nome'                     => [
                'description' => 'O nome do fornecedor.',
                'example'     => 'FORNECEDOR DE ENERGIA',
            ],
            'endereco'                 => [
                'description' => 'O endereço do fornecedor.',
                'example'     => 'Rua Principal, 200 Bairro Novo',
            ],
            'telefone'                 => [
                'description' => 'O telefone do fornecedor.',
                'example'     => '(11) 4004-2020',
            ],
            'email'                    => [
                'description' => 'O email do fornecedor.',
                'example'     => 'fornecedor@teste.com',
            ],
            'contas_pagar.*.descricao' => [
                'description' => 'A descrição da conta a pagar.',
                'example'     => 'CONTA DE ENERGIA',
            ],
            'contas_pagar.*.valor'     => [
                'description' => 'O valor da conta a pagar.',
                'example'     => 128.12,
            ],
        ];
    }
}
