<?php

namespace App\Http\Requests;

class FornecedorUpdate extends FornecedorDoc
{
    public function rules()
    {
        return [
            'nome'     => ['filled', 'max:255'],
            'endereco' => ['filled', 'max:255'],
            'telefone' => ['filled', 'max:255'],
            'email'    => ['filled', 'max:255'],
        ];
    }
}
