<?php

namespace App\Http\Requests;

class ContaPagarUpdate extends ContaPagarDoc
{
    public function rules()
    {
        return [
            'fornecedor_id' => ['filled', 'integer', 'exists:fornecedor'],
            'descricao'     => ['filled', 'max:255'],
            'valor'         => ['filled', 'numeric'],
        ];
    }
}
