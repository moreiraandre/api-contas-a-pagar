<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class ContaBancariaDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'banco_nome' => [
                'description' => 'O nome do banco.',
                'example' => 'CAIXA ECONOMICA',
            ],
            'agencia_numero' => [
                'description' => 'O número da agência.',
                'example' => '123456',
            ],
            'conta_numero' => [
                'description' => 'O número da conta.',
                'example' => '654321',
            ],
            'saldo_inicial' => [
                'description' => 'O valor do saldo inicial.',
                'example' => 1528.25,
            ],
        ];
    }
}
