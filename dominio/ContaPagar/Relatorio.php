<?php

namespace Dominio\ContaPagar;

use App\Models\ContaPagar as Model;
use Dominio\Pdf\PdfInterface;

class Relatorio
{
    public function __construct(
        private PdfInterface $pdf
    ) {}

    public function contasPendentes()
    {
        $contasPendentes = Model::whereNull('paga_em')->get();
        if (request()->header('Accept') === 'application/pdf') {
            return $this->pdf->gerar('pdf.contas-pendentes', compact('contasPendentes'));
        }
        return $contasPendentes;
    }

    public function contasLiquidadas()
    {
        $contasLiquidadas = Model::whereNotNull('paga_em')->get();
        if (request()->header('Accept') === 'application/pdf') {
            return $this->pdf->gerar('pdf.contas-liquidadas', compact('contasLiquidadas'));
        }
        return $contasLiquidadas;
    }
}
