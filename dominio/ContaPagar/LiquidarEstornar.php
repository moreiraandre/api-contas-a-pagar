<?php

namespace Dominio\ContaPagar;

use App\Models\ContaPagar as ContaPagarModel;
use Illuminate\Support\Facades\DB;

class LiquidarEstornar
{
    public function liquidar(ContaPagarModel $contaPagar): void
    {
        DB::transaction(function () use ($contaPagar) {
            $contaPagar->paga_em = now();
            $contaPagar->save();
            $contaPagar->historico()->create(['users_id' => auth()->id()]);
        });
    }

    public function estorno(ContaPagarModel $contaPagar): void
    {
        DB::transaction(function () use ($contaPagar) {
            $contaPagar->paga_em = null;
            $contaPagar->save();
            $contaPagar->historico()->create([
                'users_id'      => auth()->id(),
                'is_liquidacao' => false,
            ]);
        });
    }
}
