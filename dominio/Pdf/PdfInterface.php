<?php

namespace Dominio\Pdf;

interface PdfInterface
{
    public function gerar(string $view, array $dados, string $nomeArquivoDownload = 'arquivo.pdf');
}
