<?php

namespace Dominio\Pdf;

use PDF as Dependencia;

class Pdf implements PdfInterface
{
    public function gerar(string $view, array $dados, string $nomeArquivoDownload = 'arquivo.pdf')
    {
        $pdf = Dependencia::loadView($view, $dados);
        return $pdf->download($nomeArquivoDownload);
    }
}
