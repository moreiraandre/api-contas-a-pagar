# Conta a Pagar Relatório


## Contas Pendentes

<small class="badge badge-darkred">requires authentication</small>

Retorna a listagem de contas pendentes de pagamento. Utilize cabeçalho Accept: application/json ou application/pdf

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-pagar/relatorio/pendente" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/relatorio/pendente"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "conta_pagar_id": 15,
        "fornecedor_id": 48,
        "descricao": "CONTA DE TELEFONE",
        "valor": 156.61,
        "paga_em": null,
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:48:56.000000Z"
    },
    {
        "conta_pagar_id": 16,
        "fornecedor_id": 48,
        "descricao": "CONTA DE ENERGIA",
        "valor": 131.06,
        "paga_em": null,
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:49:23.000000Z"
    }
]
```
<div id="execution-results-GETapi-conta-pagar-relatorio-pendente" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar-relatorio-pendente"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar-relatorio-pendente"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar-relatorio-pendente" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar-relatorio-pendente"></code></pre>
</div>
<form id="form-GETapi-conta-pagar-relatorio-pendente" data-method="GET" data-path="api/conta-pagar/relatorio/pendente" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar-relatorio-pendente', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar-relatorio-pendente" onclick="tryItOut('GETapi-conta-pagar-relatorio-pendente');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar-relatorio-pendente" onclick="cancelTryOut('GETapi-conta-pagar-relatorio-pendente');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar-relatorio-pendente" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/relatorio/pendente</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar-relatorio-pendente" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar-relatorio-pendente" data-component="header"></label>
</p>
</form>


## Contas Liquidadas

<small class="badge badge-darkred">requires authentication</small>

Retorna a listagem de contas liquidadas. Utilize cabeçalho Accept: application/json ou application/pdf

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-pagar/relatorio/liquidada" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/relatorio/liquidada"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "conta_pagar_id": 15,
        "fornecedor_id": 48,
        "descricao": "CONTA DE TELEFONE",
        "valor": 156.61,
        "paga_em": "2021-04-21 06:48:56",
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:48:56.000000Z"
    },
    {
        "conta_pagar_id": 16,
        "fornecedor_id": 48,
        "descricao": "CONTA DE ENERGIA",
        "valor": 131.06,
        "paga_em": "2021-04-21 06:49:23",
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:49:23.000000Z"
    }
]
```
<div id="execution-results-GETapi-conta-pagar-relatorio-liquidada" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar-relatorio-liquidada"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar-relatorio-liquidada"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar-relatorio-liquidada" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar-relatorio-liquidada"></code></pre>
</div>
<form id="form-GETapi-conta-pagar-relatorio-liquidada" data-method="GET" data-path="api/conta-pagar/relatorio/liquidada" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar-relatorio-liquidada', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar-relatorio-liquidada" onclick="tryItOut('GETapi-conta-pagar-relatorio-liquidada');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar-relatorio-liquidada" onclick="cancelTryOut('GETapi-conta-pagar-relatorio-liquidada');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar-relatorio-liquidada" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/relatorio/liquidada</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar-relatorio-liquidada" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar-relatorio-liquidada" data-component="header"></label>
</p>
</form>



