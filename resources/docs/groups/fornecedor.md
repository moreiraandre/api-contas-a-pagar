# Fornecedor


## Listar Fornecedores

<small class="badge badge-darkred">requires authentication</small>

Retorna todos os registros do banco

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/fornecedor" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/fornecedor"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "fornecedor_id": 1,
            "nome": "Miss Oleta Morar MD",
            "endereco": "7253 Bartoletti Fall\nTreutelmouth, VT 62233",
            "telefone": "520-531-5574",
            "email": "yolanda.jerde@schumm.com",
            "created_at": "2021-04-17T02:34:58.000000Z",
            "updated_at": "2021-04-17T02:34:58.000000Z",
            "contas_pagar": [
                {
                    "conta_pagar_id": 1,
                    "fornecedor_id": "1",
                    "descricao": "veniam quia",
                    "valor": 41.93,
                    "paga_em": null,
                    "created_at": "2021-04-19T03:44:03.000000Z",
                    "updated_at": "2021-04-19T04:09:04.000000Z"
                },
                {
                    "conta_pagar_id": 3,
                    "fornecedor_id": "1",
                    "descricao": "CONTA DE TELEFONE",
                    "valor": 22.21,
                    "paga_em": null,
                    "created_at": "2021-04-19T04:24:42.000000Z",
                    "updated_at": "2021-04-19T04:24:42.000000Z"
                }
            ]
        },
        {
            "fornecedor_id": 2,
            "nome": "Mr. Robin Beahan Jr.",
            "endereco": "981 Brekke Brooks\nEast Bethel, CO 73540",
            "telefone": "971-723-2183",
            "email": "cali.jenkins@smitham.net",
            "created_at": "2021-04-17T02:35:44.000000Z",
            "updated_at": "2021-04-17T02:35:44.000000Z",
            "contas_pagar": [
                {
                    "conta_pagar_id": 1,
                    "fornecedor_id": "1",
                    "descricao": "veniam quia",
                    "valor": 41.93,
                    "paga_em": null,
                    "created_at": "2021-04-19T03:44:03.000000Z",
                    "updated_at": "2021-04-19T04:09:04.000000Z"
                },
                {
                    "conta_pagar_id": 3,
                    "fornecedor_id": "1",
                    "descricao": "CONTA DE TELEFONE",
                    "valor": 22.21,
                    "paga_em": null,
                    "created_at": "2021-04-19T04:24:42.000000Z",
                    "updated_at": "2021-04-19T04:24:42.000000Z"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/fornecedor?page=1",
        "last": "http:\/\/localhost\/api\/fornecedor?page=8",
        "prev": null,
        "next": "http:\/\/localhost\/api\/fornecedor?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 8,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=2",
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/fornecedor",
        "per_page": 5,
        "to": 5,
        "total": 36
    }
}
```
<div id="execution-results-GETapi-fornecedor" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-fornecedor"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-fornecedor"></code></pre>
</div>
<div id="execution-error-GETapi-fornecedor" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-fornecedor"></code></pre>
</div>
<form id="form-GETapi-fornecedor" data-method="GET" data-path="api/fornecedor" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-fornecedor', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-fornecedor" onclick="tryItOut('GETapi-fornecedor');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-fornecedor" onclick="cancelTryOut('GETapi-fornecedor');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-fornecedor" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/fornecedor</code></b>
</p>
<p>
<label id="auth-GETapi-fornecedor" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-fornecedor" data-component="header"></label>
</p>
</form>


## Criar Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Cria um novo fornecedor

> Example request:

```bash
curl -X POST \
    "http://localhost/api/fornecedor" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"nome":"FORNECEDOR DE ENERGIA","endereco":"Rua Principal, 200 Bairro Novo","telefone":"(11) 4004-2020","email":"fornecedor@teste.com","contas_pagar":[{"descricao":"CONTA DE ENERGIA","valor":128.12}]}'

```

```javascript
const url = new URL(
    "http://localhost/api/fornecedor"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "nome": "FORNECEDOR DE ENERGIA",
    "endereco": "Rua Principal, 200 Bairro Novo",
    "telefone": "(11) 4004-2020",
    "email": "fornecedor@teste.com",
    "contas_pagar": [
        {
            "descricao": "CONTA DE ENERGIA",
            "valor": 128.12
        }
    ]
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "nome": [
            "O campo nome é obrigatório.",
            "O campo nome não pode ser superior a 255 caracteres."
        ],
        "endereco": [
            "O campo endereco é obrigatório.",
            "O campo endereco não pode ser superior a 255 caracteres."
        ],
        "telefone": [
            "O campo telefone é obrigatório.",
            "O campo telefone não pode ser superior a 20 caracteres."
        ],
        "email": [
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ser um endereço de e-mail válido."
        ],
        "contas_pagar": [
            "O campo contas pagar deve ter um valor.",
            "O campo contas pagar deve ser uma matriz."
        ],
        "contas_pagar.0.descricao": [
            "O campo contas_pagar.0.descricao é obrigatório.",
            "O campo contas_pagar.0.descricao não pode ser superior a 255 caracteres."
        ],
        "contas_pagar.0.valor": [
            "O campo contas_pagar.0.valor é obrigatório.",
            "O campo contas_pagar.0.valor deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-fornecedor" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-fornecedor"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-fornecedor"></code></pre>
</div>
<div id="execution-error-POSTapi-fornecedor" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-fornecedor"></code></pre>
</div>
<form id="form-POSTapi-fornecedor" data-method="POST" data-path="api/fornecedor" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-fornecedor', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-fornecedor" onclick="tryItOut('POSTapi-fornecedor');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-fornecedor" onclick="cancelTryOut('POSTapi-fornecedor');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-fornecedor" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/fornecedor</code></b>
</p>
<p>
<label id="auth-POSTapi-fornecedor" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-fornecedor" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>nome</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="nome" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>endereco</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="endereco" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>telefone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="telefone" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-fornecedor" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>
<p>
<details>
<summary>
<b><code>contas_pagar</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

</summary>
<br>
<p>
<b><code>contas_pagar[].descricao</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="contas_pagar.0.descricao" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
A descrição da conta a pagar.
</p>
<p>
<b><code>contas_pagar[].valor</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="contas_pagar.0.valor" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O valor da conta a pagar.
</p>
</details>
</p>

</form>


## Detalhar Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados do fornecedor

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/fornecedor/10" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/fornecedor/10"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-GETapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-GETapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-GETapi-fornecedor--fornecedor-" data-method="GET" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-fornecedor--fornecedor-" onclick="tryItOut('GETapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-fornecedor--fornecedor-" onclick="cancelTryOut('GETapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-GETapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="GETapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
</form>


## Atualizar Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados do fornecedor

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/fornecedor/5" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"nome":"FORNECEDOR DE ENERGIA","endereco":"Rua Principal, 200 Bairro Novo","telefone":"(11) 4004-2020","email":"fornecedor@teste.com"}'

```

```javascript
const url = new URL(
    "http://localhost/api/fornecedor/5"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "nome": "FORNECEDOR DE ENERGIA",
    "endereco": "Rua Principal, 200 Bairro Novo",
    "telefone": "(11) 4004-2020",
    "email": "fornecedor@teste.com"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "nome": [
            "O campo nome deve ter um valor.",
            "O campo nome não pode ser superior a 255 caracteres."
        ],
        "endereco": [
            "O campo endereco deve ter um valor.",
            "O campo endereco não pode ser superior a 255 caracteres."
        ],
        "telefone": [
            "O campo telefone deve ter um valor.",
            "O campo telefone não pode ser superior a 20 caracteres."
        ],
        "email": [
            "O campo email deve ter um valor.",
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ser um endereço de e-mail válido."
        ]
    }
}
```
<div id="execution-results-PUTapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-PUTapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-PUTapi-fornecedor--fornecedor-" data-method="PUT" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-fornecedor--fornecedor-" onclick="tryItOut('PUTapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-fornecedor--fornecedor-" onclick="cancelTryOut('PUTapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-PUTapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>nome</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="nome" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>endereco</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="endereco" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>telefone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="telefone" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O email do fornecedor.
</p>

</form>


## Excluir Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Exclui um fornecedor

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/fornecedor/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/fornecedor/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{}
```
> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}
```
<div id="execution-results-DELETEapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-DELETEapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-DELETEapi-fornecedor--fornecedor-" data-method="DELETE" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-fornecedor--fornecedor-" onclick="tryItOut('DELETEapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-fornecedor--fornecedor-" onclick="cancelTryOut('DELETEapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-DELETEapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="DELETEapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
</form>



