# Conta a Pagar Liquidar/Estornar


## Liquidar Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Realiza a liquidação de uma conta a pagar

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/conta-pagar/liquidar/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/liquidar/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar-liquidar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar-liquidar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar-liquidar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar-liquidar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/liquidar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar-liquidar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar-liquidar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar-liquidar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar-liquidar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar-liquidar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/liquidar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar-liquidar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar-liquidar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>


## Estornar Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Realiza o estorno de uma conta a pagar

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/conta-pagar/estornar/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/estornar/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar-estornar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar-estornar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar-estornar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar-estornar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/estornar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar-estornar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar-estornar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar-estornar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar-estornar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar-estornar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/estornar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar-estornar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar-estornar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>



