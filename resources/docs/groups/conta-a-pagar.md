# Conta a Pagar


## Listar Contas a Pagar

<small class="badge badge-darkred">requires authentication</small>

Retorna todos os registros do banco

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-pagar" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "conta_pagar_id": 1,
            "descricao": "CONTA DE ENERGIA",
            "valor": 41.93,
            "paga_em": null,
            "created_at": "2021-04-19T03:44:03.000000Z",
            "updated_at": "2021-04-19T03:44:03.000000Z",
            "historico": [
                {
                    "conta_pagar_historico_id": 1,
                    "users_id": "1",
                    "is_liquidacao": "1",
                    "created_at": "2021-04-21T05:50:46.000000Z",
                    "updated_at": "2021-04-21T05:50:46.000000Z"
                },
                {
                    "conta_pagar_historico_id": 2,
                    "users_id": "1",
                    "is_liquidacao": "0",
                    "created_at": "2021-04-21T05:53:39.000000Z",
                    "updated_at": "2021-04-21T05:53:39.000000Z"
                }
            ]
        },
        {
            "conta_pagar_id": 2,
            "descricao": "CONTA DE TELEFONE",
            "valor": 180.91,
            "paga_em": null,
            "created_at": "2021-04-19T03:54:49.000000Z",
            "updated_at": "2021-04-19T03:54:49.000000Z",
            "historico": []
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/conta-pagar?page=1",
        "last": "http:\/\/localhost\/api\/conta-pagar?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/conta-pagar?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/conta-pagar",
        "per_page": 5,
        "to": 2,
        "total": 2
    }
}
```
<div id="execution-results-GETapi-conta-pagar" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar"></code></pre>
</div>
<form id="form-GETapi-conta-pagar" data-method="GET" data-path="api/conta-pagar" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar" onclick="tryItOut('GETapi-conta-pagar');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar" onclick="cancelTryOut('GETapi-conta-pagar');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar" data-component="header"></label>
</p>
</form>


## Criar Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Cria uma nova conta a pagar

> Example request:

```bash
curl -X POST \
    "http://localhost/api/conta-pagar" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"fornecedor_id":1,"descricao":"CONTA DE TELEFONE","valor":121.12}'

```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "fornecedor_id": 1,
    "descricao": "CONTA DE TELEFONE",
    "valor": 121.12
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "fornecedor_id": [
            "O campo fornecedor id é obrigatório.",
            "O campo fornecedor id deve ser um número inteiro.",
            "O campo fornecedor id selecionado é inválido."
        ],
        "descricao": [
            "O campo descricao é obrigatório.",
            "O campo descricao não pode ser superior a 255 caracteres."
        ],
        "valor": [
            "O campo valor é obrigatório.",
            "O campo valor deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-conta-pagar" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-conta-pagar"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-conta-pagar"></code></pre>
</div>
<div id="execution-error-POSTapi-conta-pagar" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-conta-pagar"></code></pre>
</div>
<form id="form-POSTapi-conta-pagar" data-method="POST" data-path="api/conta-pagar" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-conta-pagar', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-conta-pagar" onclick="tryItOut('POSTapi-conta-pagar');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-conta-pagar" onclick="cancelTryOut('POSTapi-conta-pagar');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-conta-pagar" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/conta-pagar</code></b>
</p>
<p>
<label id="auth-POSTapi-conta-pagar" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-conta-pagar" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>fornecedor_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor_id" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
O valor de fornecedor_id. No endpoint GET /api/fornecedor.
</p>
<p>
<b><code>descricao</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="descricao" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
A descrição da conta.
</p>
<p>
<b><code>valor</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="valor" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
O valor da conta.
</p>

</form>


## Detalhar Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados da conta a pagar

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-pagar/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-GETapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-GETapi-conta-pagar--conta_pagar-" data-method="GET" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar--conta_pagar-" onclick="tryItOut('GETapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('GETapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="GETapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>


## Atualizar Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados do contaPagar

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/conta-pagar/2" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"fornecedor_id":1,"descricao":"CONTA DE TELEFONE","valor":121.12}'

```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/2"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "fornecedor_id": 1,
    "descricao": "CONTA DE TELEFONE",
    "valor": 121.12
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "fornecedor_id": [
            "O campo fornecedor id deve ter um valor.",
            "O campo fornecedor id deve ser um número inteiro.",
            "O campo fornecedor id selecionado é inválido."
        ],
        "descricao": [
            "O campo descricao deve ter um valor.",
            "O campo descricao não pode ser superior a 255 caracteres."
        ],
        "valor": [
            "O campo valor deve ter um valor.",
            "O campo valor deve ser um número."
        ]
    }
}
```
<div id="execution-results-PUTapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>fornecedor_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="fornecedor_id" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
O valor de fornecedor_id. No endpoint GET /api/fornecedor.
</p>
<p>
<b><code>descricao</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="descricao" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
A descrição da conta.
</p>
<p>
<b><code>valor</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="valor" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
O valor da conta.
</p>

</form>


## Excluir Conta a Pagar

<small class="badge badge-darkred">requires authentication</small>

Exclui um contaPagar

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/conta-pagar/13" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-pagar/13"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{}
```
> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}
```
<div id="execution-results-DELETEapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-DELETEapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-DELETEapi-conta-pagar--conta_pagar-" data-method="DELETE" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-conta-pagar--conta_pagar-" onclick="tryItOut('DELETEapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('DELETEapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-DELETEapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="DELETEapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>



