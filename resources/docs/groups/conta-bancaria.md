# Conta Bancaria


## Listar Contas Bancaria

<small class="badge badge-darkred">requires authentication</small>

Retorna todos os registros do banco

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-banco" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-banco"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "conta_bancaria_id": 1,
            "banco_nome": "qui",
            "agencia_numero": "63066",
            "conta_numero": "0205534441250",
            "saldo_inicial": 145811.1021,
            "created_at": "2021-04-18T04:38:54.000000Z",
            "updated_at": "2021-04-18T04:41:21.000000Z"
        },
        {
            "conta_bancaria_id": 2,
            "banco_nome": "BANCO DO BRASIL",
            "agencia_numero": "11111111",
            "conta_numero": "222222222",
            "saldo_inicial": 1500.25,
            "created_at": "2021-04-18T04:38:54.000000Z",
            "updated_at": "2021-04-18T04:41:21.000000Z"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/fornecedor?page=1",
        "last": "http:\/\/localhost\/api\/fornecedor?page=8",
        "prev": null,
        "next": "http:\/\/localhost\/api\/fornecedor?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 8,
        "links": {
            "first": "http:\/\/localhost\/api\/conta-banco?page=1",
            "last": "http:\/\/localhost\/api\/conta-banco?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Anterior",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost\/api\/conta-banco?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Próximo &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost\/api\/conta-banco",
            "per_page": 5,
            "to": 2,
            "total": 2
        }
    }
}
```
<div id="execution-results-GETapi-conta-banco" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-banco"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-banco"></code></pre>
</div>
<div id="execution-error-GETapi-conta-banco" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-banco"></code></pre>
</div>
<form id="form-GETapi-conta-banco" data-method="GET" data-path="api/conta-banco" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-banco', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-banco" onclick="tryItOut('GETapi-conta-banco');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-banco" onclick="cancelTryOut('GETapi-conta-banco');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-banco" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-banco</code></b>
</p>
<p>
<label id="auth-GETapi-conta-banco" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-banco" data-component="header"></label>
</p>
</form>


## Criar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Cria uma nova conta bancaria

> Example request:

```bash
curl -X POST \
    "http://localhost/api/conta-banco" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"banco_nome":"CAIXA ECONOMICA","agencia_numero":"123456","conta_numero":"654321","saldo_inicial":1528.25}'

```

```javascript
const url = new URL(
    "http://localhost/api/conta-banco"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "banco_nome": "CAIXA ECONOMICA",
    "agencia_numero": "123456",
    "conta_numero": "654321",
    "saldo_inicial": 1528.25
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "banco_nome": [
            "O campo banco nome é obrigatório.",
            "O campo banco nome não pode ser superior a 255 caracteres."
        ],
        "agencia_numero": [
            "O campo agencia numero é obrigatório.",
            "O campo agencia numero não pode ser superior a 10 caracteres."
        ],
        "conta_numero": [
            "O campo conta numero é obrigatório.",
            "O campo conta numero não pode ser superior a 10 caracteres."
        ],
        "saldo_inicial": [
            "O campo saldo inicial é obrigatório.",
            "O campo saldo inicial deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-conta-banco" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-conta-banco"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-conta-banco"></code></pre>
</div>
<div id="execution-error-POSTapi-conta-banco" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-conta-banco"></code></pre>
</div>
<form id="form-POSTapi-conta-banco" data-method="POST" data-path="api/conta-banco" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-conta-banco', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-conta-banco" onclick="tryItOut('POSTapi-conta-banco');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-conta-banco" onclick="cancelTryOut('POSTapi-conta-banco');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-conta-banco" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/conta-banco</code></b>
</p>
<p>
<label id="auth-POSTapi-conta-banco" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-conta-banco" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>banco_nome</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="banco_nome" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O nome do banco.
</p>
<p>
<b><code>agencia_numero</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="agencia_numero" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O número da agência.
</p>
<p>
<b><code>conta_numero</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="conta_numero" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O número da conta.
</p>
<p>
<b><code>saldo_inicial</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="saldo_inicial" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O valor do saldo inicial.
</p>

</form>


## Detalhar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados da conta bancaria

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/conta-banco/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-banco/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}
```
<div id="execution-results-GETapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-GETapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-GETapi-conta-banco--conta_banco-" data-method="GET" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-banco--conta_banco-" onclick="tryItOut('GETapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-banco--conta_banco-" onclick="cancelTryOut('GETapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-GETapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="GETapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
</form>


## Atualizar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados da conta bancaria

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/conta-banco/20" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"banco_nome":"CAIXA ECONOMICA","agencia_numero":"123456","conta_numero":"654321","saldo_inicial":1528.25}'

```

```javascript
const url = new URL(
    "http://localhost/api/conta-banco/20"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "banco_nome": "CAIXA ECONOMICA",
    "agencia_numero": "123456",
    "conta_numero": "654321",
    "saldo_inicial": 1528.25
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}
```
> Example response (200):

```json
{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "banco_nome": [
            "O campo banco nome deve ter um valor.",
            "O campo banco nome não pode ser superior a 255 caracteres."
        ],
        "agencia_numero": [
            "O campo agencia numero deve ter um valor.",
            "O campo agencia numero não pode ser superior a 10 caracteres."
        ],
        "conta_numero": [
            "O campo conta numero deve ter um valor.",
            "O campo conta numero não pode ser superior a 10 caracteres."
        ],
        "saldo_inicial": [
            "O campo saldo inicial deve ter um valor.",
            "O campo saldo inicial deve ser um número."
        ]
    }
}
```
<div id="execution-results-PUTapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-PUTapi-conta-banco--conta_banco-" data-method="PUT" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-banco--conta_banco-" onclick="tryItOut('PUTapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-banco--conta_banco-" onclick="cancelTryOut('PUTapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>banco_nome</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="banco_nome" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O nome do banco.
</p>
<p>
<b><code>agencia_numero</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="agencia_numero" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O número da agência.
</p>
<p>
<b><code>conta_numero</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="conta_numero" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O número da conta.
</p>
<p>
<b><code>saldo_inicial</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="saldo_inicial" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O valor do saldo inicial.
</p>

</form>


## Excluir Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Exclui uma conta bancaria

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/conta-banco/7" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/conta-banco/7"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{}
```
> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}
```
<div id="execution-results-DELETEapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-DELETEapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-DELETEapi-conta-banco--conta_banco-" data-method="DELETE" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-conta-banco--conta_banco-" onclick="tryItOut('DELETEapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-conta-banco--conta_banco-" onclick="cancelTryOut('DELETEapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-DELETEapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="DELETEapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
</form>



