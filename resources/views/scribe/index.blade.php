<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI (Swagger) spec</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: April 21 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://localhost";
</script>
<script src="{{ asset("vendor/scribe/js/tryitout-2.5.3.js") }}"></script>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://localhost</code></pre><h1>Authenticating requests</h1>
<p>This API is authenticated by sending an <strong><code>Authorization</code></strong> header with the value <strong><code>"Bearer {YOUR_AUTH_KEY}"</code></strong>.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by visiting your dashboard and clicking <b>Generate API token</b>.</p><h1>Conta a Pagar Liquidar/Estornar</h1>
<h2>Liquidar Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Realiza a liquidação de uma conta a pagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/conta-pagar/liquidar/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/liquidar/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar-liquidar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar-liquidar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar-liquidar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar-liquidar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/liquidar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar-liquidar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar-liquidar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar-liquidar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar-liquidar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar-liquidar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/liquidar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar-liquidar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar-liquidar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar-liquidar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>
<h2>Estornar Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Realiza o estorno de uma conta a pagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/conta-pagar/estornar/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/estornar/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar-estornar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar-estornar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar-estornar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar-estornar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/estornar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar-estornar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar-estornar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar-estornar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar-estornar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar-estornar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/estornar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar-estornar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar-estornar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar-estornar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form><h1>Conta a Pagar Relatório</h1>
<h2>Contas Pendentes</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna a listagem de contas pendentes de pagamento. Utilize cabeçalho Accept: application/json ou application/pdf</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-pagar/relatorio/pendente" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/relatorio/pendente"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "conta_pagar_id": 15,
        "fornecedor_id": 48,
        "descricao": "CONTA DE TELEFONE",
        "valor": 156.61,
        "paga_em": null,
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:48:56.000000Z"
    },
    {
        "conta_pagar_id": 16,
        "fornecedor_id": 48,
        "descricao": "CONTA DE ENERGIA",
        "valor": 131.06,
        "paga_em": null,
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:49:23.000000Z"
    }
]</code></pre>
<div id="execution-results-GETapi-conta-pagar-relatorio-pendente" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar-relatorio-pendente"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar-relatorio-pendente"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar-relatorio-pendente" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar-relatorio-pendente"></code></pre>
</div>
<form id="form-GETapi-conta-pagar-relatorio-pendente" data-method="GET" data-path="api/conta-pagar/relatorio/pendente" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar-relatorio-pendente', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar-relatorio-pendente" onclick="tryItOut('GETapi-conta-pagar-relatorio-pendente');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar-relatorio-pendente" onclick="cancelTryOut('GETapi-conta-pagar-relatorio-pendente');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar-relatorio-pendente" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/relatorio/pendente</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar-relatorio-pendente" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar-relatorio-pendente" data-component="header"></label>
</p>
</form>
<h2>Contas Liquidadas</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna a listagem de contas liquidadas. Utilize cabeçalho Accept: application/json ou application/pdf</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-pagar/relatorio/liquidada" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/relatorio/liquidada"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "conta_pagar_id": 15,
        "fornecedor_id": 48,
        "descricao": "CONTA DE TELEFONE",
        "valor": 156.61,
        "paga_em": "2021-04-21 06:48:56",
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:48:56.000000Z"
    },
    {
        "conta_pagar_id": 16,
        "fornecedor_id": 48,
        "descricao": "CONTA DE ENERGIA",
        "valor": 131.06,
        "paga_em": "2021-04-21 06:49:23",
        "created_at": "2021-04-20T06:22:48.000000Z",
        "updated_at": "2021-04-21T06:49:23.000000Z"
    }
]</code></pre>
<div id="execution-results-GETapi-conta-pagar-relatorio-liquidada" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar-relatorio-liquidada"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar-relatorio-liquidada"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar-relatorio-liquidada" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar-relatorio-liquidada"></code></pre>
</div>
<form id="form-GETapi-conta-pagar-relatorio-liquidada" data-method="GET" data-path="api/conta-pagar/relatorio/liquidada" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar-relatorio-liquidada', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar-relatorio-liquidada" onclick="tryItOut('GETapi-conta-pagar-relatorio-liquidada');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar-relatorio-liquidada" onclick="cancelTryOut('GETapi-conta-pagar-relatorio-liquidada');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar-relatorio-liquidada" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/relatorio/liquidada</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar-relatorio-liquidada" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar-relatorio-liquidada" data-component="header"></label>
</p>
</form><h1>Conta a Pagar</h1>
<h2>Listar Contas a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna todos os registros do banco</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-pagar" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "conta_pagar_id": 1,
            "descricao": "CONTA DE ENERGIA",
            "valor": 41.93,
            "paga_em": null,
            "created_at": "2021-04-19T03:44:03.000000Z",
            "updated_at": "2021-04-19T03:44:03.000000Z",
            "historico": [
                {
                    "conta_pagar_historico_id": 1,
                    "users_id": "1",
                    "is_liquidacao": "1",
                    "created_at": "2021-04-21T05:50:46.000000Z",
                    "updated_at": "2021-04-21T05:50:46.000000Z"
                },
                {
                    "conta_pagar_historico_id": 2,
                    "users_id": "1",
                    "is_liquidacao": "0",
                    "created_at": "2021-04-21T05:53:39.000000Z",
                    "updated_at": "2021-04-21T05:53:39.000000Z"
                }
            ]
        },
        {
            "conta_pagar_id": 2,
            "descricao": "CONTA DE TELEFONE",
            "valor": 180.91,
            "paga_em": null,
            "created_at": "2021-04-19T03:54:49.000000Z",
            "updated_at": "2021-04-19T03:54:49.000000Z",
            "historico": []
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/conta-pagar?page=1",
        "last": "http:\/\/localhost\/api\/conta-pagar?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&amp;laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/conta-pagar?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &amp;raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/conta-pagar",
        "per_page": 5,
        "to": 2,
        "total": 2
    }
}</code></pre>
<div id="execution-results-GETapi-conta-pagar" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar"></code></pre>
</div>
<form id="form-GETapi-conta-pagar" data-method="GET" data-path="api/conta-pagar" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar" onclick="tryItOut('GETapi-conta-pagar');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar" onclick="cancelTryOut('GETapi-conta-pagar');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar" data-component="header"></label>
</p>
</form>
<h2>Criar Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria uma nova conta a pagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/conta-pagar" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"fornecedor_id":1,"descricao":"CONTA DE TELEFONE","valor":121.12}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "fornecedor_id": 1,
    "descricao": "CONTA DE TELEFONE",
    "valor": 121.12
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "fornecedor_id": [
            "O campo fornecedor id é obrigatório.",
            "O campo fornecedor id deve ser um número inteiro.",
            "O campo fornecedor id selecionado é inválido."
        ],
        "descricao": [
            "O campo descricao é obrigatório.",
            "O campo descricao não pode ser superior a 255 caracteres."
        ],
        "valor": [
            "O campo valor é obrigatório.",
            "O campo valor deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-conta-pagar" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-conta-pagar"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-conta-pagar"></code></pre>
</div>
<div id="execution-error-POSTapi-conta-pagar" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-conta-pagar"></code></pre>
</div>
<form id="form-POSTapi-conta-pagar" data-method="POST" data-path="api/conta-pagar" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-conta-pagar', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-conta-pagar" onclick="tryItOut('POSTapi-conta-pagar');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-conta-pagar" onclick="cancelTryOut('POSTapi-conta-pagar');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-conta-pagar" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/conta-pagar</code></b>
</p>
<p>
<label id="auth-POSTapi-conta-pagar" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-conta-pagar" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>fornecedor_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor_id" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
O valor de fornecedor_id. No endpoint GET /api/fornecedor.
</p>
<p>
<b><code>descricao</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="descricao" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
A descrição da conta.
</p>
<p>
<b><code>valor</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="valor" data-endpoint="POSTapi-conta-pagar" data-component="body" required  hidden>
<br>
O valor da conta.
</p>

</form>
<h2>Detalhar Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados da conta a pagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-pagar/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-GETapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-GETapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-GETapi-conta-pagar--conta_pagar-" data-method="GET" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-pagar--conta_pagar-" onclick="tryItOut('GETapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('GETapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-GETapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="GETapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form>
<h2>Atualizar Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados do contaPagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/conta-pagar/2" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"fornecedor_id":1,"descricao":"CONTA DE TELEFONE","valor":121.12}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/2"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "fornecedor_id": 1,
    "descricao": "CONTA DE TELEFONE",
    "valor": 121.12
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_pagar_id": 2,
        "descricao": "sit nostrum",
        "valor": 180.91,
        "paga_em": null,
        "created_at": "2021-04-19T03:54:49.000000Z",
        "updated_at": "2021-04-19T03:54:49.000000Z",
        "historico": [
            {
                "conta_pagar_historico_id": 13,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": true,
                "created_at": "2021-04-21T22:43:16.000000Z",
                "updated_at": "2021-04-21T22:43:16.000000Z"
            },
            {
                "conta_pagar_historico_id": 14,
                "users_id": 1,
                "users_name": "André Moreira",
                "is_liquidacao": false,
                "created_at": "2021-04-21T22:44:23.000000Z",
                "updated_at": "2021-04-21T22:44:23.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "fornecedor_id": [
            "O campo fornecedor id deve ter um valor.",
            "O campo fornecedor id deve ser um número inteiro.",
            "O campo fornecedor id selecionado é inválido."
        ],
        "descricao": [
            "O campo descricao deve ter um valor.",
            "O campo descricao não pode ser superior a 255 caracteres."
        ],
        "valor": [
            "O campo valor deve ter um valor.",
            "O campo valor deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-PUTapi-conta-pagar--conta_pagar-" data-method="PUT" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-pagar--conta_pagar-" onclick="tryItOut('PUTapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('PUTapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>fornecedor_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="fornecedor_id" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
O valor de fornecedor_id. No endpoint GET /api/fornecedor.
</p>
<p>
<b><code>descricao</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="descricao" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
A descrição da conta.
</p>
<p>
<b><code>valor</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="valor" data-endpoint="PUTapi-conta-pagar--conta_pagar-" data-component="body"  hidden>
<br>
O valor da conta.
</p>

</form>
<h2>Excluir Conta a Pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui um contaPagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://localhost/api/conta-pagar/13" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-pagar/13"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{}</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaPagar] 4"
}</code></pre>
<div id="execution-results-DELETEapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-conta-pagar--conta_pagar-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<div id="execution-error-DELETEapi-conta-pagar--conta_pagar-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-conta-pagar--conta_pagar-"></code></pre>
</div>
<form id="form-DELETEapi-conta-pagar--conta_pagar-" data-method="DELETE" data-path="api/conta-pagar/{conta_pagar}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-conta-pagar--conta_pagar-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-conta-pagar--conta_pagar-" onclick="tryItOut('DELETEapi-conta-pagar--conta_pagar-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-conta-pagar--conta_pagar-" onclick="cancelTryOut('DELETEapi-conta-pagar--conta_pagar-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-conta-pagar--conta_pagar-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/conta-pagar/{conta_pagar}</code></b>
</p>
<p>
<label id="auth-DELETEapi-conta-pagar--conta_pagar-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-conta-pagar--conta_pagar-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_pagar</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_pagar" data-endpoint="DELETEapi-conta-pagar--conta_pagar-" data-component="url" required  hidden>
<br>
O valor de conta_pagar_id
</p>
</form><h1>Conta Bancaria</h1>
<h2>Listar Contas Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna todos os registros do banco</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-banco" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-banco"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "conta_bancaria_id": 1,
            "banco_nome": "qui",
            "agencia_numero": "63066",
            "conta_numero": "0205534441250",
            "saldo_inicial": 145811.1021,
            "created_at": "2021-04-18T04:38:54.000000Z",
            "updated_at": "2021-04-18T04:41:21.000000Z"
        },
        {
            "conta_bancaria_id": 2,
            "banco_nome": "BANCO DO BRASIL",
            "agencia_numero": "11111111",
            "conta_numero": "222222222",
            "saldo_inicial": 1500.25,
            "created_at": "2021-04-18T04:38:54.000000Z",
            "updated_at": "2021-04-18T04:41:21.000000Z"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/fornecedor?page=1",
        "last": "http:\/\/localhost\/api\/fornecedor?page=8",
        "prev": null,
        "next": "http:\/\/localhost\/api\/fornecedor?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 8,
        "links": {
            "first": "http:\/\/localhost\/api\/conta-banco?page=1",
            "last": "http:\/\/localhost\/api\/conta-banco?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&amp;laquo; Anterior",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost\/api\/conta-banco?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Próximo &amp;raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost\/api\/conta-banco",
            "per_page": 5,
            "to": 2,
            "total": 2
        }
    }
}</code></pre>
<div id="execution-results-GETapi-conta-banco" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-banco"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-banco"></code></pre>
</div>
<div id="execution-error-GETapi-conta-banco" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-banco"></code></pre>
</div>
<form id="form-GETapi-conta-banco" data-method="GET" data-path="api/conta-banco" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-banco', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-banco" onclick="tryItOut('GETapi-conta-banco');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-banco" onclick="cancelTryOut('GETapi-conta-banco');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-banco" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-banco</code></b>
</p>
<p>
<label id="auth-GETapi-conta-banco" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-banco" data-component="header"></label>
</p>
</form>
<h2>Criar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria uma nova conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/conta-banco" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"banco_nome":"CAIXA ECONOMICA","agencia_numero":"123456","conta_numero":"654321","saldo_inicial":1528.25}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-banco"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "banco_nome": "CAIXA ECONOMICA",
    "agencia_numero": "123456",
    "conta_numero": "654321",
    "saldo_inicial": 1528.25
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "banco_nome": [
            "O campo banco nome é obrigatório.",
            "O campo banco nome não pode ser superior a 255 caracteres."
        ],
        "agencia_numero": [
            "O campo agencia numero é obrigatório.",
            "O campo agencia numero não pode ser superior a 10 caracteres."
        ],
        "conta_numero": [
            "O campo conta numero é obrigatório.",
            "O campo conta numero não pode ser superior a 10 caracteres."
        ],
        "saldo_inicial": [
            "O campo saldo inicial é obrigatório.",
            "O campo saldo inicial deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-conta-banco" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-conta-banco"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-conta-banco"></code></pre>
</div>
<div id="execution-error-POSTapi-conta-banco" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-conta-banco"></code></pre>
</div>
<form id="form-POSTapi-conta-banco" data-method="POST" data-path="api/conta-banco" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-conta-banco', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-conta-banco" onclick="tryItOut('POSTapi-conta-banco');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-conta-banco" onclick="cancelTryOut('POSTapi-conta-banco');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-conta-banco" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/conta-banco</code></b>
</p>
<p>
<label id="auth-POSTapi-conta-banco" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-conta-banco" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>banco_nome</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="banco_nome" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O nome do banco.
</p>
<p>
<b><code>agencia_numero</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="agencia_numero" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O número da agência.
</p>
<p>
<b><code>conta_numero</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="conta_numero" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O número da conta.
</p>
<p>
<b><code>saldo_inicial</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="saldo_inicial" data-endpoint="POSTapi-conta-banco" data-component="body" required  hidden>
<br>
O valor do saldo inicial.
</p>

</form>
<h2>Detalhar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados da conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/conta-banco/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-banco/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}</code></pre>
<div id="execution-results-GETapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-GETapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-GETapi-conta-banco--conta_banco-" data-method="GET" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-conta-banco--conta_banco-" onclick="tryItOut('GETapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-conta-banco--conta_banco-" onclick="cancelTryOut('GETapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-GETapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="GETapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
</form>
<h2>Atualizar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados da conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/conta-banco/20" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"banco_nome":"CAIXA ECONOMICA","agencia_numero":"123456","conta_numero":"654321","saldo_inicial":1528.25}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-banco/20"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "banco_nome": "CAIXA ECONOMICA",
    "agencia_numero": "123456",
    "conta_numero": "654321",
    "saldo_inicial": 1528.25
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "conta_bancaria_id": 2,
        "banco_nome": "BANCO DO BRASIL",
        "agencia_numero": "11111111",
        "conta_numero": "222222222",
        "saldo_inicial": 1500.25,
        "created_at": "2021-04-18T04:38:54.000000Z",
        "updated_at": "2021-04-18T04:41:21.000000Z"
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "banco_nome": [
            "O campo banco nome deve ter um valor.",
            "O campo banco nome não pode ser superior a 255 caracteres."
        ],
        "agencia_numero": [
            "O campo agencia numero deve ter um valor.",
            "O campo agencia numero não pode ser superior a 10 caracteres."
        ],
        "conta_numero": [
            "O campo conta numero deve ter um valor.",
            "O campo conta numero não pode ser superior a 10 caracteres."
        ],
        "saldo_inicial": [
            "O campo saldo inicial deve ter um valor.",
            "O campo saldo inicial deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-PUTapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-PUTapi-conta-banco--conta_banco-" data-method="PUT" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-conta-banco--conta_banco-" onclick="tryItOut('PUTapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-conta-banco--conta_banco-" onclick="cancelTryOut('PUTapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-PUTapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>banco_nome</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="banco_nome" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O nome do banco.
</p>
<p>
<b><code>agencia_numero</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="agencia_numero" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O número da agência.
</p>
<p>
<b><code>conta_numero</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="conta_numero" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O número da conta.
</p>
<p>
<b><code>saldo_inicial</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="saldo_inicial" data-endpoint="PUTapi-conta-banco--conta_banco-" data-component="body"  hidden>
<br>
O valor do saldo inicial.
</p>

</form>
<h2>Excluir Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui uma conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://localhost/api/conta-banco/7" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/conta-banco/7"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{}</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\ContaBancaria] 4"
}</code></pre>
<div id="execution-results-DELETEapi-conta-banco--conta_banco-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-conta-banco--conta_banco-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-conta-banco--conta_banco-"></code></pre>
</div>
<div id="execution-error-DELETEapi-conta-banco--conta_banco-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-conta-banco--conta_banco-"></code></pre>
</div>
<form id="form-DELETEapi-conta-banco--conta_banco-" data-method="DELETE" data-path="api/conta-banco/{conta_banco}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-conta-banco--conta_banco-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-conta-banco--conta_banco-" onclick="tryItOut('DELETEapi-conta-banco--conta_banco-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-conta-banco--conta_banco-" onclick="cancelTryOut('DELETEapi-conta-banco--conta_banco-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-conta-banco--conta_banco-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/conta-banco/{conta_banco}</code></b>
</p>
<p>
<label id="auth-DELETEapi-conta-banco--conta_banco-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-conta-banco--conta_banco-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>conta_banco</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="conta_banco" data-endpoint="DELETEapi-conta-banco--conta_banco-" data-component="url" required  hidden>
<br>
O valor de conta_bancaria_id
</p>
</form><h1>Endpoints</h1>
<h2>Listagem de usuários</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna todos os usuários cadastrados</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/user" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/user"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "id": 1,
        "name": "André Moreira",
        "email": "andre.mcx1@gmail.com",
        "created_at": "2021-04-17T01:55:05.000000Z",
        "updated_at": "2021-04-17T01:55:05.000000Z"
    },
    {
        "id": 2,
        "name": "Rosamond Rosenbaum",
        "email": "dtrantow@example.com",
        "created_at": "2021-04-17T02:04:33.000000Z",
        "updated_at": "2021-04-17T02:04:33.000000Z"
    }
]</code></pre>
<div id="execution-results-GETapi-user" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user"></code></pre>
</div>
<div id="execution-error-GETapi-user" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user"></code></pre>
</div>
<form id="form-GETapi-user" data-method="GET" data-path="api/user" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user" onclick="tryItOut('GETapi-user');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user" onclick="cancelTryOut('GETapi-user');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user</code></b>
</p>
<p>
<label id="auth-GETapi-user" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-user" data-component="header"></label>
</p>
</form><h1>Fornecedor</h1>
<h2>Listar Fornecedores</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna todos os registros do banco</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/fornecedor" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/fornecedor"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "fornecedor_id": 1,
            "nome": "Miss Oleta Morar MD",
            "endereco": "7253 Bartoletti Fall\nTreutelmouth, VT 62233",
            "telefone": "520-531-5574",
            "email": "yolanda.jerde@schumm.com",
            "created_at": "2021-04-17T02:34:58.000000Z",
            "updated_at": "2021-04-17T02:34:58.000000Z",
            "contas_pagar": [
                {
                    "conta_pagar_id": 1,
                    "fornecedor_id": "1",
                    "descricao": "veniam quia",
                    "valor": 41.93,
                    "paga_em": null,
                    "created_at": "2021-04-19T03:44:03.000000Z",
                    "updated_at": "2021-04-19T04:09:04.000000Z"
                },
                {
                    "conta_pagar_id": 3,
                    "fornecedor_id": "1",
                    "descricao": "CONTA DE TELEFONE",
                    "valor": 22.21,
                    "paga_em": null,
                    "created_at": "2021-04-19T04:24:42.000000Z",
                    "updated_at": "2021-04-19T04:24:42.000000Z"
                }
            ]
        },
        {
            "fornecedor_id": 2,
            "nome": "Mr. Robin Beahan Jr.",
            "endereco": "981 Brekke Brooks\nEast Bethel, CO 73540",
            "telefone": "971-723-2183",
            "email": "cali.jenkins@smitham.net",
            "created_at": "2021-04-17T02:35:44.000000Z",
            "updated_at": "2021-04-17T02:35:44.000000Z",
            "contas_pagar": [
                {
                    "conta_pagar_id": 1,
                    "fornecedor_id": "1",
                    "descricao": "veniam quia",
                    "valor": 41.93,
                    "paga_em": null,
                    "created_at": "2021-04-19T03:44:03.000000Z",
                    "updated_at": "2021-04-19T04:09:04.000000Z"
                },
                {
                    "conta_pagar_id": 3,
                    "fornecedor_id": "1",
                    "descricao": "CONTA DE TELEFONE",
                    "valor": 22.21,
                    "paga_em": null,
                    "created_at": "2021-04-19T04:24:42.000000Z",
                    "updated_at": "2021-04-19T04:24:42.000000Z"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/fornecedor?page=1",
        "last": "http:\/\/localhost\/api\/fornecedor?page=8",
        "prev": null,
        "next": "http:\/\/localhost\/api\/fornecedor?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 8,
        "links": [
            {
                "url": null,
                "label": "&amp;laquo; Previous",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/fornecedor?page=2",
                "label": "Next &amp;raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/fornecedor",
        "per_page": 5,
        "to": 5,
        "total": 36
    }
}</code></pre>
<div id="execution-results-GETapi-fornecedor" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-fornecedor"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-fornecedor"></code></pre>
</div>
<div id="execution-error-GETapi-fornecedor" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-fornecedor"></code></pre>
</div>
<form id="form-GETapi-fornecedor" data-method="GET" data-path="api/fornecedor" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-fornecedor', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-fornecedor" onclick="tryItOut('GETapi-fornecedor');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-fornecedor" onclick="cancelTryOut('GETapi-fornecedor');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-fornecedor" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/fornecedor</code></b>
</p>
<p>
<label id="auth-GETapi-fornecedor" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-fornecedor" data-component="header"></label>
</p>
</form>
<h2>Criar Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria um novo fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/fornecedor" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"nome":"FORNECEDOR DE ENERGIA","endereco":"Rua Principal, 200 Bairro Novo","telefone":"(11) 4004-2020","email":"fornecedor@teste.com","contas_pagar":[{"descricao":"CONTA DE ENERGIA","valor":128.12}]}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/fornecedor"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "nome": "FORNECEDOR DE ENERGIA",
    "endereco": "Rua Principal, 200 Bairro Novo",
    "telefone": "(11) 4004-2020",
    "email": "fornecedor@teste.com",
    "contas_pagar": [
        {
            "descricao": "CONTA DE ENERGIA",
            "valor": 128.12
        }
    ]
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "nome": [
            "O campo nome é obrigatório.",
            "O campo nome não pode ser superior a 255 caracteres."
        ],
        "endereco": [
            "O campo endereco é obrigatório.",
            "O campo endereco não pode ser superior a 255 caracteres."
        ],
        "telefone": [
            "O campo telefone é obrigatório.",
            "O campo telefone não pode ser superior a 20 caracteres."
        ],
        "email": [
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ser um endereço de e-mail válido."
        ],
        "contas_pagar": [
            "O campo contas pagar deve ter um valor.",
            "O campo contas pagar deve ser uma matriz."
        ],
        "contas_pagar.0.descricao": [
            "O campo contas_pagar.0.descricao é obrigatório.",
            "O campo contas_pagar.0.descricao não pode ser superior a 255 caracteres."
        ],
        "contas_pagar.0.valor": [
            "O campo contas_pagar.0.valor é obrigatório.",
            "O campo contas_pagar.0.valor deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-fornecedor" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-fornecedor"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-fornecedor"></code></pre>
</div>
<div id="execution-error-POSTapi-fornecedor" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-fornecedor"></code></pre>
</div>
<form id="form-POSTapi-fornecedor" data-method="POST" data-path="api/fornecedor" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-fornecedor', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-fornecedor" onclick="tryItOut('POSTapi-fornecedor');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-fornecedor" onclick="cancelTryOut('POSTapi-fornecedor');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-fornecedor" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/fornecedor</code></b>
</p>
<p>
<label id="auth-POSTapi-fornecedor" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-fornecedor" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>nome</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="nome" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>endereco</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="endereco" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>telefone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="telefone" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-fornecedor" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>
<p>
<details>
<summary>
<b><code>contas_pagar</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

</summary>
<br>
<p>
<b><code>contas_pagar[].descricao</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="contas_pagar.0.descricao" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
A descrição da conta a pagar.
</p>
<p>
<b><code>contas_pagar[].valor</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="contas_pagar.0.valor" data-endpoint="POSTapi-fornecedor" data-component="body" required  hidden>
<br>
O valor da conta a pagar.
</p>
</details>
</p>

</form>
<h2>Detalhar Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados do fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/fornecedor/10" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/fornecedor/10"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-GETapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-GETapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-GETapi-fornecedor--fornecedor-" data-method="GET" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-fornecedor--fornecedor-" onclick="tryItOut('GETapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-fornecedor--fornecedor-" onclick="cancelTryOut('GETapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-GETapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="GETapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
</form>
<h2>Atualizar Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados do fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/fornecedor/5" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"nome":"FORNECEDOR DE ENERGIA","endereco":"Rua Principal, 200 Bairro Novo","telefone":"(11) 4004-2020","email":"fornecedor@teste.com"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/fornecedor/5"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "nome": "FORNECEDOR DE ENERGIA",
    "endereco": "Rua Principal, 200 Bairro Novo",
    "telefone": "(11) 4004-2020",
    "email": "fornecedor@teste.com"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "fornecedor_id": 38,
        "nome": "Nome Teste",
        "endereco": "Rua principal",
        "telefone": "9999999999",
        "email": "email@teste.com",
        "created_at": "2021-04-17T03:37:53.000000Z",
        "updated_at": "2021-04-17T03:37:53.000000Z",
        "contas_pagar": [
            {
                "conta_pagar_id": 1,
                "fornecedor_id": "1",
                "descricao": "veniam quia",
                "valor": 41.93,
                "paga_em": null,
                "created_at": "2021-04-19T03:44:03.000000Z",
                "updated_at": "2021-04-19T04:09:04.000000Z"
            },
            {
                "conta_pagar_id": 3,
                "fornecedor_id": "1",
                "descricao": "CONTA DE TELEFONE",
                "valor": 22.21,
                "paga_em": null,
                "created_at": "2021-04-19T04:24:42.000000Z",
                "updated_at": "2021-04-19T04:24:42.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "nome": [
            "O campo nome deve ter um valor.",
            "O campo nome não pode ser superior a 255 caracteres."
        ],
        "endereco": [
            "O campo endereco deve ter um valor.",
            "O campo endereco não pode ser superior a 255 caracteres."
        ],
        "telefone": [
            "O campo telefone deve ter um valor.",
            "O campo telefone não pode ser superior a 20 caracteres."
        ],
        "email": [
            "O campo email deve ter um valor.",
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ser um endereço de e-mail válido."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-PUTapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-PUTapi-fornecedor--fornecedor-" data-method="PUT" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-fornecedor--fornecedor-" onclick="tryItOut('PUTapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-fornecedor--fornecedor-" onclick="cancelTryOut('PUTapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-PUTapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>nome</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="nome" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>endereco</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="endereco" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>telefone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="telefone" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-fornecedor--fornecedor-" data-component="body"  hidden>
<br>
O email do fornecedor.
</p>

</form>
<h2>Excluir Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui um fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://localhost/api/fornecedor/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/fornecedor/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{}</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Fornecedor] 4"
}</code></pre>
<div id="execution-results-DELETEapi-fornecedor--fornecedor-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-fornecedor--fornecedor-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-fornecedor--fornecedor-"></code></pre>
</div>
<div id="execution-error-DELETEapi-fornecedor--fornecedor-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-fornecedor--fornecedor-"></code></pre>
</div>
<form id="form-DELETEapi-fornecedor--fornecedor-" data-method="DELETE" data-path="api/fornecedor/{fornecedor}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-fornecedor--fornecedor-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-fornecedor--fornecedor-" onclick="tryItOut('DELETEapi-fornecedor--fornecedor-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-fornecedor--fornecedor-" onclick="cancelTryOut('DELETEapi-fornecedor--fornecedor-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-fornecedor--fornecedor-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/fornecedor/{fornecedor}</code></b>
</p>
<p>
<label id="auth-DELETEapi-fornecedor--fornecedor-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-fornecedor--fornecedor-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>fornecedor</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="fornecedor" data-endpoint="DELETEapi-fornecedor--fornecedor-" data-component="url" required  hidden>
<br>
O valor de fornecedor_id
</p>
</form>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>