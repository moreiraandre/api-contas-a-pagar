<h1 style="text-align: center">RELATÓRIO - CONTAS PENDENTES</h1>

<table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
    <thead>
    <tr>
        <th>Descrição</th>
        <th>Valor</th>
        <th>Criada em</th>
    </tr>
    </thead>

    <tbody>
    @foreach($contasPendentes as $conta)
        <tr>
            <td>{{$conta->descricao}}</td>
            <td>{{$conta->valor}}</td>
            <td>{{$conta->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
