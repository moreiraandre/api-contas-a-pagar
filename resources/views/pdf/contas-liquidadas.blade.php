<h1 style="text-align: center">RELATÓRIO - CONTAS LIQUIDADAS</h1>

<table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
    <thead>
    <tr>
        <th>Descrição</th>
        <th>Valor</th>
        <th>Criada em</th>
        <th>Paga em</th>
        <th>Usuário</th>
    </tr>
    </thead>

    <tbody>
    @foreach($contasLiquidadas as $conta)
        <tr>
            <td>{{$conta->descricao}}</td>
            <td>{{$conta->valor}}</td>
            <td>{{$conta->created_at->format('d/m/Y h:i:s')}}</td>
            <td>{{$conta->paga_em->format('d/m/Y h:i:s')}}</td>
            <td>{{$conta->historico()->latest()->first()->usuario->name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
