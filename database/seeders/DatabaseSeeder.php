<?php

namespace Database\Seeders;

use App\Models\ContaPagar;
use App\Models\ContaPagarHistorico;
use App\Models\Fornecedor;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Fornecedor::factory(10)
            ->has(
                ContaPagar::factory(['paga_em' => now()])
                    ->count(3)
                    ->has(ContaPagarHistorico::factory(), 'historico'),
                'contasPagar'
            )
            ->create();

        Fornecedor::factory(10)
            ->has(
                ContaPagar::factory()
                    ->count(3),
                'contasPagar'
            )
            ->create();
    }
}
