<?php

namespace Database\Factories;

use App\Models\ContaPagarHistorico;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContaPagarHistoricoFactory extends Factory
{
    protected $model = ContaPagarHistorico::class;

    public function definition()
    {
        return [
            'users_id' => User::inRandomOrder()->first()->getKey(),
        ];
    }
}
