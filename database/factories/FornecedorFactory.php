<?php

namespace Database\Factories;

use App\Models\Fornecedor;
use Illuminate\Database\Eloquent\Factories\Factory;

class FornecedorFactory extends Factory
{
    protected $model = Fornecedor::class;

    public function definition()
    {
        return [
            'nome'     => $this->faker->name,
            'endereco' => $this->faker->address,
            'telefone' => $this->faker->phoneNumber,
            'email'    => $this->faker->email,
        ];
    }
}
