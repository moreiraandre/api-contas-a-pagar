<?php

namespace Database\Factories;

use App\Models\ContaBancaria;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContaBancariaFactory extends Factory
{
    protected $model = ContaBancaria::class;

    public function definition()
    {
        return [
            'banco_nome'     => $this->faker->word,
            'agencia_numero' => $this->faker->randomNumber(5),
            'conta_numero'   => $this->faker->randomNumber(5),
            'saldo_inicial'  => $this->faker->randomFloat(),
        ];
    }
}
