<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContaPagarHistoricoTable extends Migration
{
    public function up()
    {
        Schema::create('conta_pagar_historico', function (Blueprint $table) {
            $table->id('conta_pagar_historico_id');

            $table->foreignId('users_id')->constrained();

            $table->unsignedBigInteger('conta_pagar_id');
            $table->foreign('conta_pagar_id')->references('conta_pagar_id')->on('conta_pagar');

            $table->boolean('is_liquidacao')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conta_pagar_historico');
    }
}
