<?php

namespace {{ namespace }};

use {{ namespacedModel }} as Model;
use {{ rootNamespace }}Http\Controllers\Controller;
use {{ rootNamespace }}Http\Resources\{{ model }} as Resource;
use {{ rootNamespace }}Http\Requests\{{ model }}Store;
use {{ rootNamespace }}Http\Requests\{{ model }}Update;

class {{ class }} extends Controller
{
    /**
     * Listar {{ model }}
     *
     * Retorna todos os registros do banco
     * @group {{ model }}
     * @responseFile Response/{{ model }}/Listar.json
     */
    public function index()
    {
        return Resource::collection(Model::paginate(5));
    }

    /**
     * Criar {{ model }}
     *
     * Cria um novo {{ modelVariable }}
     * @group {{ model }}
     * @responseFile 201 Response/{{ model }}/Detalhar.json
     * @responseFile 422 Response/{{ model }}/ValidarCriar.json
     */
    public function store({{ model }}Store $request)
    {
        $novo = Model::create($request->all());
        return new Resource($novo);
    }

    /**
     * Detalhar {{ model }}
     *
     * Retorna os dados do {{ modelVariable }}
     * @group {{ model }}
     * @urlParam {{ modelVariable }} integer required O valor de {{ modelVariable }}_id
     * @responseFile Response/{{ model }}/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\{{ model }}] 4"}
     */
    public function show(Model ${{ modelVariable }})
    {
        return new Resource(${{ modelVariable }});
    }

    /**
     * Atualizar {{ model }}
     *
     * Atualiza os dados do {{ modelVariable }}
     * @group {{ model }}
     * @urlParam {{ modelVariable }} integer required O valor de {{ modelVariable }}_id
     * @responseFile Response/{{ model }}/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\{{ model }}] 4"}
     * @responseFile 422 Response/{{ model }}/ValidarAtualizar.json
     */
    public function update({{ model }}Update $request, Model ${{ modelVariable }})
    {
        ${{ modelVariable }}->update($request->all());
        return new Resource(${{ modelVariable }});
    }

    /**
     * Excluir {{ model }}
     *
     * Exclui um {{ modelVariable }}
     * @group {{ model }}
     * @urlParam {{ modelVariable }} integer required O valor de {{ modelVariable }}_id
     * @response
     * @response 404 {"message": "No query results for model [App\\Models\\{{ model }}] 4"}
     */
    public function destroy(Model ${{ modelVariable }})
    {
        ${{ modelVariable }}->delete();
    }
}
