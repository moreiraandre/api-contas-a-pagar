<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;

/**
 * Listagem de usuários
 *
 * Retorna todos os usuários cadastrados
 * @responseFile Response/UsuariosListar.json
 */
Route::middleware('auth:sanctum')->get('/user', function () {
    return User::all();
});


Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('fornecedor', 'FornecedorController');
    Route::apiResource('conta-banco', 'ContaBancariaController');
    Route::apiResource('conta-pagar', 'ContaPagarController');
    Route::put('conta-pagar/liquidar/{conta_pagar}', 'ContaPagarLiquidarController');
    Route::put('conta-pagar/estornar/{conta_pagar}', 'ContaPagarEstornarController');
    Route::get('conta-pagar/relatorio/pendente', 'ContaPagarRelatorioController@contasPendentes')->name('relatorio.contas-pendentes');
    Route::get('conta-pagar/relatorio/liquidada', 'ContaPagarRelatorioController@contasLiquidadas');
});
