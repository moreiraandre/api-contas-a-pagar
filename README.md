# API Contas a Pagar com Laravel
André Moreira <andre.mcx1@gmail.com>

Não esqueça de deixar a estrela no repositório pra incentivar a produção de conteúdo. Fique a vontade pra fazer o Fork 
e abrir issues caso ache necessário.

# Clone e instalação da Aplicação
```bash
git clone git@gitlab.com:moreiraandre/api-contas-a-pagar.git www && \
cd www && \
./installapp
```

# Comandos docker
É necessário que você está dentro da pasta laradock, que está na raiz do projeto.

### Iniciar serviços
```bash
docker-compose up -d nginx
```

### Listar serviços ativos
```bash
docker-compose ps
```

### Entrar no terminal ZSH do container Workspace
```bash
docker-compose exec --user=laradock workspace zsh
```

### Parar serviços
```bash
docker-compose down
```
> É necessário que você esteja fora do terminal do container.

# Tutorial
https://www.youtube.com/watch?v=tTIWaOMGB5I&list=PLeYEvoL1bU0Yaqg5s0qDrKXvOVXG6WF91
